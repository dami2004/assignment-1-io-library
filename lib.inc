section .text
 
 


 ;1) 15.09.22
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60  
    syscall ;system call







;2)15.09.22
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax ;i=0
    .loop: ;while(str[i]!=0) i++
        cmp byte[rax+rdi],0 ;if end -> go to .end
        je .end
        inc rax ;i++
        jmp .loop
    .end:
        ret



;3)15.09.22
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rdx, rax ;rdx = len(str)
    mov rsi, rdi ;rsi is string addr
    mov rdi, 1 ; stdout
    mov rax, 1 
    syscall
    ret




;4)16.09.22
; Принимает код символа и выводит его в stdout
print_char:
    push rdi 
    mov rdx, 1
    mov rsi, rsp 
    mov rax, 1      
    mov rdi, 1      
    syscall         ; systemcall
    pop rdi
    ret



;5)16.09.22
; Переводит строку (выводит символ с кодом 0xA)
print_newline:

    mov rdi, '\n' 
    jmp print_char





;6)20.09.22
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r8, 10 
    mov r9, rsp 
    mov rax, rdi ;rax = N
   
    push 0 ; '0' is the end of str
    .loop:
        xor rdx,rdx ;DR
        div r8 
        add rdx, '0' ;char-> ASCII
        dec rsp 
        mov byte[rsp], dl 
        cmp rax,0 
        je .end
        jmp .loop 

    .end:
        mov rdi, rsp 
        push r9
        call print_string
        pop r9
        mov rsp, r9 
        ret




;7)20.09.22
; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    cmp rdi,0
    jge print_uint 


    .neg:
        push rdi  ;save num
        mov rdi, '-' 
        call print_char
        pop rdi
        neg rdi 
        jmp print_uint 




;8)20.09.22
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx ;i
    xor r8, r8 ;clear regs
    xor r9, r9
    .loop: ;r8b <-  8 bit char
        mov r8b, byte[rdi+rcx] ;r8 = s1[i]
        mov r9b, byte[rsi+rcx] ;r9 = s2[i]
        cmp r8b,r9b
        jne .no ;if s1[i]!=s2[i] return  false
        cmp r8b,0
        je .yes ;if s1[i]==s2[i]=='0' return true
        inc rcx ;i++
        jmp .loop

    .yes:
        mov rax, 1   ;return true
        ret
    .no:
        mov rax,0 ;return false
        ret





;9)23.09.22
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ;rax=0 -> input
    xor rdi,rdi
    mov rdx, 1 ;len = 1
    push 0 
    mov rsi, rsp ;load char addr
    syscall
    pop rax ;load shar from stack to rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор





;10)23.09.22
read_word:
    xor rcx, rcx
    xor rax, rax


    .loop:

        push rcx
        push rsi
        push rdi
        call read_char 
        pop rdi
        pop rsi
        pop rcx

        cmp rax, 0 ;if end go to .end
        je .end

        cmp rax, ' ' 
        je .skip_whitespace
        cmp rax, 0x9
        je .skip_whitespace
        cmp rax, '\n'
        je .skip_whitespace

        mov [rdi+rcx], rax  ;  word[i]=char
        inc rcx ;i++
        cmp rcx, rsi ;if(i>buffer_length) overflow error
        jge .error

        jmp .loop


    .error:
        xor rax, rax ;return nulls 
        xor rdx, rdx
        ret
    .skip_whitespace:
        cmp rcx,0
        je .loop
    .end:
        xor rax, rax
        mov [rdi+rcx], rax ;last null. s[i+1] = '0'
        mov rax, rdi ;addr
        mov rdx, rcx ;len
        ret




;11)21.09.22
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10 
    .loop: ;movzx - extend with 0
        movzx r9, byte[rdi+rcx] ;r9 = s[i]
        cmp r9,0 ;if r9==0 end
        je .end
        cmp r9b, '0' 
        jl .end     ;parse if not a num
        cmp r9b, '9'
        jg .end

        mul r8
        sub r9b, '0' 
        add rax, r9 ;rax = rax*10 + digit. 
        inc rcx ;i++
        jmp .loop

    .end
        mov rdx, rcx ;rdx = i
        ret





;12)21.09.22
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx ;clear regs

    cmp byte[rdi], '-'
    je .neg
    jmp .pos 
    .neg:
        inc rdi 

        push rdi
        call parse_uint 
        pop rdi
        neg rax 
        inc rdx 
        ret
    .pos:

        jmp parse_uint 



;13)23.09.22
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy: ;args: rdi, rsi, rdx
    xor rax, rax
    xor rcx, rcx ;i=0
    push rsi
    push rdi
    
    push rdx
    call string_length
    pop rdx
    
    pop rdi
    pop rsi
    inc rax

    cmp rdx, rax 
    jl .error
    .loop:
        cmp rcx, rax;if(i> len(str)) break;
        jg .end
        mov r10b,[rdi+rcx] ;buffer[i]=string[i]
        mov [rsi+rcx], r10b
        inc rcx; i++
        jmp .loop

    .error:
        xor rax, rax ;return 0
        ret
    .end:

        ret

